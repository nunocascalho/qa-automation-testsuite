# Resumo

* Executado na fase generate-test-resources com o goal generate após serem criados os runners  
* Cria ficheiro TestSuite.xml com o template já conhecido  
* Configurações do projeto  
	* Output directory - pasta onde será guardado o ficheiro TestSuite.xml  
	* Sources directory - pasta (package) onde se encontram os runners  
	* Test sources root - pasta raiz para código de teste  
* Configurações do ficheiro  
	* _threadCount_ - numero inteiro que determina quantas tarefas executarão os testes em paralelo  
	* _features_ - mapa de testes a serem inseridos no ficheiro TestSuite  
		* _feature_  
			* _name_ - nome do teste (ex: Esign, Process Continuity, etc)  
			* _enabled_ - indica se o teste é inserido ou não no TestSuite (ex: true/false)


# Inclusão no ficheiro pom.xml

````
<build>
	<plugins>
		<plugin>
			<groupId>qateam</groupId>
			<artifactId>testsuite-maven-plugin</artifactId>
			<version>1.0-SNAPSHOT</version>
			<executions>
				<execution>
					<goals>
						<goal>generate</goal>
					</goals>
					<phase>generate-test-resources</phase>
				</execution>
			</executions>
			<configuration>
				<threadCount>4</threadCount>
				<features>
					<feature>
						<name>Process Continuity</name>
					</feature>
					<feature>
						<name>Esign</name>
						<enabled>false</enabled>
					</feature>
				</features>
			</configuration>
		</plugin>
	</plugins>
</build>
````
