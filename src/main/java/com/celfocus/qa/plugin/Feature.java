package com.celfocus.qa.plugin;

public class Feature {

    /**
     * @parameter
     *      property="name"
     * @required
     */
    private String name;

    /**
     * @parameter
     *      property="enabled"
     *      default-value="true"
     */
    private boolean enabled = true;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
