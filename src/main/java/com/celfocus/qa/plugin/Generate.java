package com.celfocus.qa.plugin;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Generate TestSuite.xml file
 *
 * @goal generate
 * @phase generate-feature-resources
 */
public class Generate extends AbstractMojo {

    /**
     * Folder for TestSuite file
     * Default value is 'src/test/suites/'
     *
     * @parameter
     *      property="outputDirectory"
     *      default-value="src/test/suites/"
     */
    //private File outputDirectory;
    private String outputDirectory;

    /**
     * Folder containing runners
     * Default value is 'celfocus/omnichannel/runners/'
     *
     * @parameter
     *      property="sourcesDirectory"
     *      default-value="celfocus/omnichannel/runners/"
     */
    //private File sourcesDirectory;
    private String sourcesDirectory;

    /**
     * Root folder for feature sources
     * Default value is 'src/feature/java/'
     *
     * @parameter
     *      property="testSourcesRoot"
     *      default-value="src/test/java/"
     */
    //private File testSourcesRoot;
    private String testSourcesRoot;

    /**
     * Defines the 'thread-count' value on TestSuite.xml
     * If not specified the default value is 1.
     *
     * @parameter
     *      property="threadCount"
     *      default-value=1
     */
    private Integer threadCount;

    /**
     * Defines the tests inside the TestSuite
     * If not specified no tests will be written
     *
     * @parameter
     *      property="features"
     */
    private List<Feature> features;

    private final String filename = "TestSuite.xml";

    public void execute() throws MojoExecutionException, MojoFailureException {

        /**
         * Filter features to handle
         */
        Set<String> enabledFeatures = new HashSet<String>();
        for (Feature feature : features) {
            if (feature.isEnabled())
                enabledFeatures.add(feature.getName());
        }

        SourcesHandler sourcesHandler = new SourcesHandler();
        sourcesHandler.setLog( getLog() );
        sourcesHandler.setFeatures( enabledFeatures );
        sourcesHandler.setOutputDirectory( outputDirectory );
        sourcesHandler.setSourcesDirectory( sourcesDirectory );
        sourcesHandler.setTestSourcesRoot( testSourcesRoot );

        /**
         * Call handler to read sources
         */
        Map<String, Set<String>> sources = null;
        try {
            sources = sourcesHandler.readSources();
        } catch (IOException e) {
            e.printStackTrace();
        }


        /**
         * Create the output directory & TestSuite file
         */
        FileWriter writer = sourcesHandler.createFile(filename);

        if (writer == null)
            return;

        /**
         * Populate file with sources
         */
        try {
            sourcesHandler.writeFile( writer, threadCount, sources);
        } catch (IOException e) {
            getLog().error(new MojoExecutionException( "Error writing to file.", e ));
            throw new MojoExecutionException( "Error writing to file.", e );
        }

        try {
            writer.close();
            getLog().info(filename + " was created with success");
        } catch (IOException e) {
            getLog().error(new MojoExecutionException( "Error closing file writer " + writer, e ));
            throw new MojoExecutionException( "Error closing file writer " + writer, e );
        }

    }

}
