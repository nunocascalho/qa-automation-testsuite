package com.celfocus.qa.plugin;

import com.celfocus.qa.temyers.FeatureFileClassNamingScheme;
import org.apache.commons.io.FilenameUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class SourcesHandler {

    private static final String REGEX_FILTER_FILENAME = "_\\d+.java";

    /**
     * Log handler
     */
    private Log log;

    /**
     * A set of unique features
     */
    private Set<String> features;

    /**
     * The output directory
     */
    private String outputDirectory;

    /**
     * The sources directory
     */
    private String sourcesDirectory;

    /**
     * The test root for Java sources
     */
    private String testSourcesRoot;

    public void setLog(Log log) {
        this.log = log;
    }

    public void setFeatures(Set<String> features) {
        this.features = features;
    }

    public void setOutputDirectory(String outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    public void setSourcesDirectory(String sourcesDirectory) {
        this.sourcesDirectory = sourcesDirectory;
    }

    public void setTestSourcesRoot(String testSourcesRoot) {
        this.testSourcesRoot = testSourcesRoot;
    }

    /**
     * Read runners
     *
     * @return
     */
    public Map<String,Set<String>> readSources() throws IOException {

        String feature;
        File f = new File(testSourcesRoot+sourcesDirectory);

        if ( !f.exists() )
        {
            log.error("Source folder (" + f.getCanonicalPath() + ") does not exist.");
            return null;
        }

        File[] files = f.listFiles();
        Map<String,Set<String>> filesPerFeatures = initSourcesDatabase();

        for (File file : files) {

            feature = findFeature( file.getName(), features);

            if (feature != null)
                filesPerFeatures.get(feature)
                                .add(
                                    FilenameUtils.removeExtension(file.getName())
                                );

        }

        return filesPerFeatures;

    }

    /**
     * Init sources map - set of files by feature
     *
     * @return
     */
    private Map<String, Set<String>> initSourcesDatabase() {
        Map<String, Set<String>> map = new HashMap<String, Set<String>>();

        for (String feature : features) {
            map.put(feature, new TreeSet<String>());
        }

        return map;
    }

    /**
     * Find the feature associated to the runner filename
     *
     * @param filename
     * @return
     */
    private String findFeature( String filename, Set<String> features) {

        for (String feature : features) {
            if (validateFilenameWithFeature(filename,feature))
                return feature;
        }

        return null;

    }

    /**
     * Handle
     * @param filename
     * @param feature
     * @return
     */
    private boolean validateFilenameWithFeature(String filename, String feature) {

        FeatureFileClassNamingScheme scheme = new FeatureFileClassNamingScheme();

        String lcFilename = filename.toLowerCase().replaceAll(REGEX_FILTER_FILENAME,"");
        String lcFeature = scheme.generate(feature).toLowerCase();

        return lcFilename.equalsIgnoreCase(lcFeature);
    }

    /**
     * Creates TestSuite.xml file
     *
     * @return
     * @throws MojoExecutionException
     */
    public FileWriter createFile(String filename) throws MojoExecutionException {

        File f = new File(outputDirectory);

        if ( !f.exists() )
        {
            f.mkdirs();
        }

        File suite = new File( f, filename);

        FileWriter writer = null;

        try {
            writer = new FileWriter(suite);
        } catch (IOException e) {
            throw new MojoExecutionException( "Error creating file " + suite, e );
        }

        return writer;

    }

    /**
     * Fill the TestSuite file
     * @param writer
     * @param threadCount
     * @throws IOException
     */
    public void writeFile(FileWriter writer, Integer threadCount, Map<String, Set<String>> sources) throws IOException {

        XMLSuiteBuilder builder = new XMLSuiteBuilder(log);
        builder.setWriter(writer);
        builder.setFeatures(sources);
        builder.setRunnersPackage(sourcesDirectory);
        builder.setThreadCount(threadCount);

        builder.build();

    }
}
