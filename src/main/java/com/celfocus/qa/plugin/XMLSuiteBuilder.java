package com.celfocus.qa.plugin;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import org.apache.maven.plugin.logging.Log;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.ConnectException;
import java.util.Map;
import java.util.Set;

public class XMLSuiteBuilder {

    private Log log;

    private FileWriter writer;
    private Map< String, Set<String>> features;
    private String runnersPackage;
    private Integer threadCount;

    public XMLSuiteBuilder(Log log) {
        this.log = log;
    }

    public FileWriter getWriter() {
        return writer;
    }

    public void setWriter(FileWriter writer) {
        this.writer = writer;
    }

    public Map<String, Set<String>> getFeatures() {
        return features;
    }

    public void setFeatures(Map<String, Set<String>> features) {
        this.features = features;
    }

    public String getRunnersPackage() {
        return runnersPackage;
    }

    public void setRunnersPackage(String runnersPackage) {
        this.runnersPackage = runnersPackage;
    }

    public void setThreadCount(Integer threadCount) {
        this.threadCount = threadCount;
    }

    public void build() throws IOException {

        StringBuilder builder = new StringBuilder();

        /**
         * Build Header
         */
        builder.append( getXMLHeader() );

        /**
         * Build Body
         */
        builder.append( getXMLBody() );

        /**
         * Pretty print to file
         */
        writer.append( getXMLPreHeader() );
        writer.append(
            getXMLPrettyPrint(builder.toString())
        );

    }

    private String getXMLPreHeader() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<!DOCTYPE suite SYSTEM \"http://testng.org/testng-1.0.dtd\">\n";
    }

    private String getXMLHeader() {
        return "<suite name =\"Suite\" parallel=\"classes\" preserve-order=\"true\" thread-count=\"" + threadCount + "\" >";
    }

    /**
     * Build the dynamic body of the XML document
     * @return
     */
    private String getXMLBody() {

        StringBuilder builder = new StringBuilder();

        for (String feature : features.keySet()) {

            if (!features.get(feature).isEmpty()){
                builder.append( buildFeatureTest(feature) );
            }

        }

        builder.append("</suite>");

        return builder.toString();
    }

    private String buildFeatureTest(String feature) {

        StringBuilder builder = new StringBuilder();

        builder.append( "<test name=\"" + feature + "\">" +
                        "<classes>");

        for (String runner : features.get(feature)) {
            builder.append( "<class name=\"" +
                            runnersPackage.replace("/",".") + runner +
                            "\" />");
        }

        builder.append( "</classes>" +
                        "</test>");

        return builder.toString();
    }

    /**
     * Identation of XML Document
     * @param unformatedXml
     * @return
     */
    private String getXMLPrettyPrint(String unformatedXml) {

        StringWriter output = null;
        DocumentBuilderFactory factory;
        DocumentBuilder db;
        Document doc;
        OutputFormat format;
        XMLSerializer serializer;


        try {
            factory = DocumentBuilderFactory.newInstance();

            db = factory.newDocumentBuilder();
            doc = db.parse(new InputSource(new StringReader(unformatedXml)));

            format = new OutputFormat(doc);
            format.setIndenting(true);
            format.setIndent(2);
            format.setOmitXMLDeclaration(true);
            format.setLineWidth(Integer.MAX_VALUE);

            output = new StringWriter();
            serializer = new XMLSerializer(output, format);
            serializer.serialize(doc);

        } catch (ConnectException e) {
            if (log != null)
                log.error("Problems due to connection.");
            else
                e.printStackTrace();
        } catch (Exception e) {
            if (log != null)
                log.error(e);
            else
                e.printStackTrace();
        }

        return output.toString();

    }

    public static void main(String[] args) {

        String test =   "<!DOCTYPE suite SYSTEM \"http://testng.org/testng-1.0.dtd\">\n" +
                "<suite name=\"Suite\" parallel=\"classes\" preserve-order=\"true\" thread-count=\"4\">\n" +
                "<test name=\"Process Continuity\">\n" +
                "<classes>\n" +
                "<class name=\"celfocus.omnichannel.runners.Processcontinuity01IT\"/>\n" +
                "<class name=\"celfocus.omnichannel.runners.Processcontinuity02IT\"/>\n" +
                "</classes>\n" +
                "</test>\n" +
                "</suite>";

        System.out.println(test);

        XMLSuiteBuilder builder = new XMLSuiteBuilder(null);
        System.out.println(builder.getXMLPrettyPrint(test));
    }

}
