package com.celfocus.qa.temyers;

public interface ClassNamingScheme {
    String generate(final String featureFileName);
}
