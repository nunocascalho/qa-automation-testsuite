# Changelog

## 1.0.1
* **Fix** feature filename (from contains to equals)
    * Feature filename handled just like Temyers plugin rules
* **Changed** groupId from *qateam* to *com.celfocus.qa*